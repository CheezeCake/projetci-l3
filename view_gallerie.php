<?php

if(!isset($_GET['id']))
{
	echo 'Pas d\'id recu';
	exit();
}

include 'admin/logged.php';
include 'admin/connect.php';

$query = 'SELECT name, id_owner FROM galleries WHERE id='.$_GET['id'];
$result = pg_query($DB, $query);

?>

<!DOCTYPE html>
<html>
	<head>
		<title>Galleries</title>
		<meta charset="utf-8">
		<link rel="stylesheet" href="css/header.css">
		<link rel="stylesheet" href="css/body.css">
		<link rel="stylesheet" href="css/footer.css">
		<link rel="stylesheet" href="js/reveal/reveal.css">
		<link rel="stylesheet" href="js/dropit/dropit.css">
		<script tyep="text/javascript" src="js/jquery-1.8.3.min.js"></script>
		<script type="text/javascript" src="js/reveal/jquery.reveal.js"></script>
		<script type="text/javascript" src="js/dropit/dropit.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	    $('.menu').dropit();
});
</script>
	</head>
	<body id="page-body">
	<!-- header -->
<?php include 'header.php'; ?>
	<!-- /header -->
		<section>
			<div class="box" id="full-page-box">
				<table>
<?php
if($result)
{
	$data = pg_fetch_assoc($result);
	$id_owner = $data['id_owner'];
	echo '<tr><td colspan="4" class="title-2">'.$data['name'].'</td></tr>';

	$query = 'SELECT id, path, name FROM photos WHERE id_gallerie='.$_GET['id'];
	$result = pg_query($DB, $query);

	$i = 0;
	while($data = pg_fetch_assoc($result))
	{
		if($i%4 == 0)
			echo '<tr>';

		echo '<td><img src="'.$data['path'].'" alt="'.$data['name'].'"></td>';

		if(++$i%4 == 0)
			echo '</tr>';
	}

	if($i%4 != 1)
		echo '</tr>';

	if($LOGGED && $id_owner == $_SESSION['id'])
		echo '<tr><td colspan="4"><a href="#" data-reveal-id="upload-box">Ajouter des photos</a></td></tr>';
}
else
	echo '<tr><td>Cette galerie n\'existe pas !</td></tr>';
?>
				</table>
			</div>
			<div id="upload-box" class="reveal-modal">
				<h1>Reveal Modal Goodness</h1>
				<p>This is a default modal in all its glory, but any of the styles here can easily be changed in the CSS.</p>
				<a class="close-reveal-modal">&#215;</a>
			</div>
		</section>
	</body>
</html>
