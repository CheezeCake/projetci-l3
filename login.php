<?php
include 'admin/logged.php';

if($LOGGED)
	header('location: admin/index.php');
?>

<!DOCTYPE html>
<html>
	<head>
		<title>Connexion</title>
		<link rel="stylesheet" href="css/header.css">
		<link rel="stylesheet" href="css/body.css">
		<link rel="stylesheet" href="css/footer.css">
	</head>
	<body id="page-body">
	<!-- header -->
<?php include 'header.php'; ?>
	<!-- /header -->
		<section>
			<div id="signup-form" class="box">
				<h3 class="title-4">Connexion</h3>
				<hr>
				<form method="post" action="admin/do_login.php">
				<table>
					<tr><?php
						if(isset($_GET['error']))
							echo '<td rowspan="2" class="error">Identifiant ou<br>mot de passe<br>incorrect(s)</td>';
						?>
						<td>Identifiant</td><td><input type="text" name="login"></td>
					</tr>
					<tr>
						<td>Mot de passe</td><td><input type="password" name="password"></td>
					</tr>
					<tr>
						<td></td><td><input type="submit" value="se connecter"></td>
					</tr>
				</table>
				</form>
			</div>
		</section>
	</body>
</html>
