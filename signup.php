<?php
include 'admin/logged.php';

if($LOGGED)
	header('location: admin/index.php');
?>

<!DOCTYPE html>
<html>
	<head>
		<title>Inscription</title>
		<link rel="stylesheet" href="css/header.css">
		<link rel="stylesheet" href="css/body.css">
		<link rel="stylesheet" href="css/footer.css">
	</head>
	<body id="page-body">
	<!-- header -->
<?php include 'header.php'; ?>
	<!-- /header -->
		<section>
			<div id="signup-form" class="box">
				<h3 class="title-4">Inscription</h3>
				<hr>
				<form method="post" action="admin/do_signup.php" enctype="multipart/form-data">
				<table>
					<tr>
						<td class="error"><?php if(isset($_GET['email'])) echo 'Invalide';?></td>
						<td>E-mail</td>
						<td><input type="text" name="email"></td>
					</tr>
					<tr>
						<td class="error"><?php
							if(isset($_GET['login']))
							{
								if($_GET['login'] == 'used')
									echo 'D&eacute;j&agrave; utilis&eacute';
								else
									echo 'Erreur';
							}
						?></td>
						<td>Identifiant (30 caract&egrave;res max)</td><td><input type="text" name="login"></td>
					</tr>
					<tr>
						<td class="error"><?php if(isset($_GET['password'])) echo 'Erreur';?></td>
						<td>Mot de passe (30 caract&egrave;res max)</td><td><input type="password" name="password"></td>
					</tr>
					<tr>
						<td class="error"><?php if(isset($_GET['age'])) echo 'Erreur';?></td>
						<td>Age</td><td><input type="number" name="age" min="0" step="1"></td>
					</tr>
					<tr>
						<td class="error"><?php
							if(isset($_GET['avatar']))
							{
								switch($_GET['avatar'])
								{
									case 'size':
										echo '1Mo max !';
										break;
									case 'extension':
										echo 'extension non support&eacute;e';
										break;
									default:
										echo 'Erreur';
								}
							}
						?></td>
						<td>Avatar</td><td><input type="file" name="avatar"></td>
					</tr>
					<tr>
						<td></td>
						<td></td><td><input type="submit" value="s'inscrire"></td>
					</tr>
				</table>
				</form>
			</div>
		</section>
	</body>
</html>

