<?php

include 'admin/logged.php';

if($LOGGED)
	header('location: admin/index.php');

?>

<!Doctype html>
<html>
	<head>
		<title>PixBucket</title>
		<link rel="stylesheet" href="css/header.css">
		<link rel="stylesheet" href="css/body.css">
		<link rel="stylesheet" href="css/footer.css">
		<meta charset="utf-8">
	</head>
	<body id="homepage-body">
	<!-- header -->
<?php include 'header.php'; ?>
	<!-- /header -->
	<!-- page -->
<?php include 'home.html'; ?>
	<!-- /page -->
	<!-- footer -->
<?php include 'footer.html'; ?>
	<!-- /footer -->
	</body>
</html>
