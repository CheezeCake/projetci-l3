<?php

/*
 * Si ce script reçois un id en POST, il n'affiche que les galleries
 * de l'utilisateur identifié par cet id.
 *
 * Sinon il affiche TOUTES les galleries presentes sur le site
 */

include 'admin/logged.php';
include 'admin/connect.php';

$one = (!empty($_GET['id']));

if($one)
{
	$id = $_GET['id'];

	$query = 'SELECT login FROM users_projet WHERE id = '.$id;
	$result = pg_query($DB, $query);
}

?>

<!DOCTYPE html>
<html>
	<head>
		<title>Galleries</title>
		<meta charset="utf-8">
		<link rel="stylesheet" href="css/header.css">
		<link rel="stylesheet" href="css/body.css">
		<link rel="stylesheet" href="css/footer.css">
		<link rel="stylesheet" href="js/reveal/reveal.css">
		<script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
		<script type="text/javascript" src="js/reveal/jquery.reveal.js"></script>
		<link rel="stylesheet" href="js/dropit/dropit.css">
		<script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
		<script type="text/javascript" src="js/dropit/dropit.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	    $('.menu').dropit();
});
</script>
<script type="text/javascript">

function create_gallerie() {
	var xhr = new XMLHttpRequest();

	xhr.onreadystatechange = function() {
		if(xhr.readyState == 4) {
			var label = document.getElementById('label');
			label.innerHTML = 'Nom de la Galerie: ';

			switch (xhr.responseText)
			{
				case 'used':
					label.innerHTML += '<span class="error"> Vous avez déjà une gallerie de ce nom</span>';
					break;
				case 'fail':
					label.innerHTML += '<span class="error"> Erreur</span>';
					break;
				default:
					window.location = 'view_gallerie.php?id='+xhr.responseText;
					break;
			}
		}
	}

	var name = document.getElementById('input');
	xhr.open("POST", "admin/create_gallerie.php", true);
	xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xhr.send("name="+encodeURIComponent(name.value));
}

</script>
	</head>
	<body id="page-body">
	<!-- header -->
<?php include 'header.php'; ?>
	<!-- /header -->
		<section>
			<div class="box" id="full-page-box">
				<table class="gallerie-preview">
					<tr colspan="4">
						<td class="title-3">Galleries<?php if($one) { $data = pg_fetch_assoc($result); echo ' de '.$data['login']; } ?></td>
					</tr>
<?php

$query = 'SELECT id, name FROM galleries';
if($one)
	$query .= ' WHERE id_owner='.$id;

$result = pg_query($DB, $query);

$i = 0;
while($data = pg_fetch_assoc($result))
{
	if($i%4 == 0)
		echo '<tr>';

	$query = 'SELECT path FROM photos WHERE id_gallerie='.$data['id'].' LIMIT 1';
	$result_photo = pg_query($DB, $query);
	$photo = pg_fetch_assoc($result_photo);


	echo '<td class="gallerie-preview-cell" style="background-image: url(\'galleries/';
	if(empty($photo))
		echo 'empty.png\');">';
	else
		echo $photo['path'].'\');">';

	echo '<a href="view_gallerie.php?id='.$data['id'].'">'.$data['name'].'</a></td>';

	if(++$i%4 == 0)
		echo '</tr>';
}

if($i%4 != 1)
	echo '</tr>';
?>
<?php
if($one && $LOGGED && $_GET['id'] == $_SESSION['id'])
{
?>

					<tr>
						<td class="add-link"><a href="#" data-reveal-id="upload-box">Nouvelle galerie</a></td>
					</tr>
<?php
}
?>
				</table>
			</div>
			<div id="upload-box" class="reveal-modal">
				<label id="label">Nom de la galerie: </label><input id="input" type="text">
				<input onclick="create_gallerie()" type="submit" value="créer">
				<a class="close-reveal-modal">&#215;</a>
			</div>
		</section>
	</body>
</html>
