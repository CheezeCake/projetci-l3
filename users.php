<?php

include 'admin/connect.php';
include 'admin/logged.php';

$query = 'SELECT id, login, avatar FROM users_projet';
$result = pg_query($DB, $query);

?>

<!DOCTYPE html>
<html>
	<head>
		<title>Galleries</title>
		<link rel="stylesheet" href="css/header.css">
		<link rel="stylesheet" href="css/body.css">
		<link rel="stylesheet" href="css/footer.css">
		<link rel="stylesheet" href="js/dropit/dropit.css">
		<script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
		<script type="text/javascript" src="js/dropit/dropit.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	    $('.menu').dropit();
});
</script>
	</head>
	<body id="page-body">
	<!-- header -->
<?php include 'header.php'; ?>
	<!-- /header -->
		<section>
			<div class="box" id="full-page-box">
				<table>
<?php
	while($data = pg_fetch_assoc($result))
	{
		echo '<tr><td><img width="100" height="100" src="avatars/'.$data['avatar'].'"></td><td><a href="view_profile.php?id='.$data['id'].'">'.$data['login'].'</a></td></tr>';
	}
?>
				</ul>
			</div>
		</section>
	</body>
</html>
