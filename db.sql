CREATE TABLE users_projet(
	id SERIAL,
	login VARCHAR(30),
	email VARCHAR(256),
	password VARCHAR(256),
	age SMALLINT,
	avatar VARCHAR(256)
);

CREATE TABLE galleries(
	id SERIAL,
	id_owner INTEGER, -- users_projet(id)
	name VARCHAR(80)
);

CREATE TABLE photos(
	id SERIAL,
	id_gallerie INTEGER, -- galleries(id)
	path TEXT,
	name VARCHAR(256),
	tags TEXT
);
