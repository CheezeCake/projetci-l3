<?php

include 'admin/connect.php';
include 'admin/logged.php';

if(empty($_GET['id']))
{
	header('location: users.php');
	exit();
}

$query = 'SELECT login, avatar, age FROM users_projet WHERE id='.$_GET['id'];
$result = pg_query($DB, $query);

?>

<!DOCTYPE html>
<html>
	<head>
		<title>Galleries</title>
		<link rel="stylesheet" href="css/header.css">
		<link rel="stylesheet" href="css/body.css">
		<link rel="stylesheet" href="css/footer.css">
		<link rel="stylesheet" href="js/dropit/dropit.css">
		<script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
		<script type="text/javascript" src="js/dropit/dropit.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	    $('.menu').dropit();
});
</script>
	</head>
	<body id="page-body">
	<!-- header -->
<?php include 'header.php'; ?>
	<!-- /header -->
		<section>
			<div class="box" id="med-box">
<?php
	$data = pg_fetch_assoc($result);
?>
				<table>
					<tr>
						<td><?php echo $data['login']; ?></td>
						<td><img width="100" height="100" src="avatars/<?php echo $data['avatar']; ?>"></td>
					</tr>
					<tr>
						<td class="title-section">Age</td>
						<td><?php echo $data['age']; ?></td>
					</tr>
					<tr>
						<td>Ses galeries</td>
						<td><a href="galleries.php?id=<?php echo $_GET['id']; ?>">Galeries de <?php echo $data['login']; ?></a></td>
					</tr>
				</table>
			</div>
		</section>
	</body>
</html>
