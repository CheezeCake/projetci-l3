<?php

/*
 * POST:
 * login, password
 */

$login = $_POST['login'];
$password = $_POST['password'];

if(!empty($login) && !empty($password))
{
	include 'connect.php';

	$query = 'SELECT id, password, avatar FROM users_projet WHERE login LIKE \''.$login.'\'';
	$result = pg_query($DB, $query);

	if($result === false)
	{
		header('location: index.php?error=login');
		die();
	}

	$data = pg_fetch_assoc($result);
	if(strcmp($data['password'], sha1($password)) != 0)
	{
		header('location: login.php?error=pass');
		die();
	}

	// connexion ok
	session_start();
	$_SESSION['login'] = $login;
	$_SESSION['id'] = $data['id'];
	$_SESSION['avatar'] = $data['avatar'];

	header('location: admin/index.php');
}

header('location: login.php?error=nodata');
