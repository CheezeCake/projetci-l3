<?php

include 'verif_logged.php';
include 'connect.php';

$query = 'SELECT id, name FROM galleries WHERE id_owner='.$_SESSION['id'].' LIMIT 3';
$result = pg_query($DB, $query);

?>

<!DOCTYPE html>
<html>
	<head>
		<title>Home</title>
		<link rel="stylesheet" href="../css/header.css">
		<link rel="stylesheet" href="../css/body.css">
		<link rel="stylesheet" href="../css/footer.css">
		<link rel="stylesheet" href="../js/dropit/dropit.css">
		<script type="text/javascript" src="../js/jquery-1.8.3.min.js"></script>
		<script type="text/javascript" src="../js/dropit/dropit.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	    $('.menu').dropit();
});
</script>
	</head>
	<body id="page-body">
	<!-- header -->
<?php include 'header.php'; ?>
	<!-- /header -->
		<section>
			<div class="box" id="home-gal-list">
				<table class="gallerie-preview">
					<tr>
					<td><a href="../galleries.php?id=<?php echo $_SESSION['id']; ?>"><h5 class="title-4">Vos derni&egrave;res galeries</h5></a></td>
					</tr>
<?php

$i = 0;
while($data = pg_fetch_assoc($result))
{
	$query = 'SELECT id, path FROM photos WHERE id_gallerie = '.$data['id'].' LIMIT 1';
	$result_photo = pg_query($DB, $query);
	$photo = pg_fetch_assoc($result_photo);

	echo '<tr><td class="gallerie-preview-cell" style="background-image: url(\'../galleries/';
	if(empty($photo))
		echo 'empty.png\');">';
	else
		echo $photo['path'].'\');">';

	echo '<a href="../view_gallerie.php?id='.$data['id'].'">'.$data['name'].'</a></td></tr>';
	$i++;
}

?>
				</table>
			</div>
		</section>
	</body>
</html>
