<header role="banner">
	<div>
		<div id="header-left"><a id="logo" href="index.php">PixBucket</a> <a href="../users.php">Utilisateurs</a> <a href="../galleries.php">Galeries</a></div>
		<div id="header-right">
				<?php
				if($LOGGED)
				{
					//echo '<a href="profile.php">'.$_SESSION['login'].'</a>';
				?>
					<ul class="menu">
						<li>
							<a href="#"><?php echo $_SESSION['login']; ?></a>
							<ul id="submenu">
								<li><a href="profile.php">Mon compte</a></li>
								<li><a href="../galleries.php?id=<?php echo $_SESSION['id']; ?>">Mes Galleries</a></li>
							</ul>
						</li>
					</ul>
					<span>&nbsp;</span>
					<a href="logout.php">D&eacute;connexion</a>
				<?php
				}
				else
				{
				?>
					<a id="login-link" href="../login.php">Connexion</a>
					<a id="signup-link" href="../signup.php">Inscription</a>
				<?php
				}
				?>
		</div>
	</div>
</header>
