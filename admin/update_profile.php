<?php

include 'verif_logged.php';
include 'connect.php';

/*
 * GET:
 * avatar
 * delavatar
 * pass
 *
 * POST:
 * oldpassword
 * newpassword
 * newpassword1
 *
 * FILES:
 * avatar
 */

if(isset($_GET['avatar']))
{
	if(!empty($_FILES['avatar']['name']))
	{
		if($_FILES['avatar']['error'] > 0)
		{
			header('location: profile.php?avatar=error');
			die();
		}
		else if($_FILES['avatar']['name']/1024 > 1024) // 1Mo max
		{
			header('location: profile.php?avatar=size');
			die();
		}

		$allowed_exts = array('jpg', 'jpeg', 'png', 'gif', 'bmp');
		$ext = end(explode('.', $_FILES['avatar']['name']));
		if(!in_array($ext, $allowed_exts))
		{
			header('location: profile.php?avatar=extension');
			die();
		}

		$avatar = $_SESSION['login'].'.'.$ext;

		// rm ancien
		$query = 'SELECT avatar FROM users_projet WHERE id='.$_SESSION['id'];
		$result = pg_query($DB, $query);
		$data = pg_fetch_assoc($result);
		if(strcmp($_SESSION['avatar'], $default) != 0)
			unlink('../avatars/'.$data['avatar']);

		move_uploaded_file($_FILES['avatar']['tmp_name'], '../avatars/'.$avatar);
		$query = 'UPDATE users_projet SET avatar=\''.$avatar.'\' WHERE id='.$_SESSION['id'];
		pg_query($DB, $query);

		$_SESSION['avatar'] = $avatar;
		header('location: profile.php');
		die();
	}
}

if(isset($_GET['delavatar']))
{
	$default = 'default/default.png';

	$query = 'SELECT avatar FROM users_projet WHERE id='.$_SESSION['id'];
	$result = pg_query($DB, $query);
	$data = pg_fetch_assoc($result);

	if(strcmp($data['avatar'], $default) != 0)
	{
		unlink('../avatars/'.$data['avatar']);
		$query = 'UPDATE users_projet SET avatar=\''.$default.'\' WHERE id='.$_SESSION['id'];
		pg_query($DB, $query);

		$_SESSION['avatar'] = $default;
	}

	header('location: profile.php');
	die();
}

if(isset($_GET['pass']))
{
	if(!empty($_POST['oldpassword']) && !empty($_POST['newpassword']) && !empty($_POST['newpassword1']))
	{
		$old = $_POST['oldpassword'];
		$new = $_POST['newpassword'];
		$new1 = $_POST['newpassword1'];

		if(strcmp($new, $new1) == 0)
		{
			$query = 'SELECT password FROM users_projet WHERE id='.$_SESSION['id'];
			$result = pg_query($DB, $query);
			$data = pg_fetch_assoc($result);

			if(strcmp(sha1($old), $data['password']) == 0)
			{
				$query = 'UPDATE users_projet SET password=\''.$sha1($newpassword).'\' WHERE id='.$_SESSION['id'];
				pg_query($DB, $query);
				header('location: profile.php');
				die();
			}
		}
	}
}

header('location: profile.php?error');
