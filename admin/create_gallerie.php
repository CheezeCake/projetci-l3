<?php

include 'verif_logged.php';
include 'connect.php';

function used_name($name, $db)
{
	$query = 'SELECT * FROM galleries WHERE name LIKE \''.$name.'\' AND id_owner='.$_SESSION['id'];
	$result = pg_query($db, $query);

	return (pg_num_rows($result) == 1);
}

/*
 * POST:
 * name
 */

header("Content-Type: text/plain");

if(!empty($_POST['name']))
{
	if(!used_name($_POST['name'], $DB))
	{
		$query = 'INSERT INTO galleries (id_owner, name) VALUES('.$_SESSION['id'].', \''.$_POST['name'].'\')';
		$result = pg_query($DB, $query);

		if(!$result)
			echo 'fail';
		else
		{
			$query = 'SELECT id FROM galleries WHERE id_owner='.$_SESSION['id'].'AND name LIKE \''.$_POST['name'].'\'';
			$result = pg_query($DB, $query);
			$data = pg_fetch_assoc($result);

			mkdir('../galleries/'.$data['id']);
			echo $data['id'];
		}
	}
	else
		echo 'used';
}
else
	echo 'fail';
