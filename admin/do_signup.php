<?php

include 'logged.php';
if($LOGGED)
{
	header('location: index.php');
	exit();
}

include 'connect.php';

function used_login($login, $db)
{
	$query = 'SELECT * FROM users_projet WHERE login LIKE \''.$login.'\'';
	$result = pg_query($db, $query);

	return (pg_num_rows($result) == 1);
}

/*
 * POST:
 * email, login, password, age
 *
 * FILES:
 * avatar
 */

$email = $_POST['email'];
$login = $_POST['login'];
$password = $_POST['password'];
$age = $_POST['age'];
$avatar = 'default/default.png';

$errors = array(
	'email' => (empty($email) || !preg_match('/[a-z]+@[a-z]+\.[a-z]{2,}/', $email)
		|| strlen($email) > 256),
			'login' => (empty($login) || strlen($login) > 30 /*|| !preg_match('/^\S$/', $login)*/),
	'password' => (empty($password) || strlen($password) > 30),
	'age' => (empty($age) || !is_numeric($age))
);

$redirect_url = '../signup.php?';

foreach($errors as $key => $val)
{
	if($val)
		$redirect_url .= $key.'&';
}

if(used_login($login, $DB))
	$redirect_url .= 'login=used&';

$redirect_url = rtrim($redirect_url, '&?');
if(strcmp($redirect_url, '../signup.php') != 0)
{
	header('location: '.$redirect_url);
	die();
}


/*
 * Upload avatar
 */
$avatar_submited = false;
if(!empty($_FILES['avatar']['name']))
{
	if($_FILES['avatar']['error'] > 0)
	{
		header('location: ../signup.php?avatar=error');
		die();
	}
	else if($_FILES['avatar']['size']/1024 > 1024) // 1Mo max
	{
		header('location: ../signup.php?avatar=size');
		die();
	}

	$allowed_exts = array('jpg', 'jpeg', 'png', 'gif', 'bmp');
	$ext = end(explode('.', $_FILES['avatar']['name']));
	if(!in_array($ext, $allowed_exts))
	{
		header('location: ../signup.php?avatar=extension');
		die();
	}

	$avatar = $login.'.'.$ext;
	$avatar_submited = true;
}


/*
 * Aucunes erreurs dans les données
 * envoyés par l'utilisateur
 */
$query = 'INSERT INTO users_projet (email, login, password, age, avatar) VALUES(\''.
	$email.'\', \''.$login.'\', \''.sha1($password).'\', '.$age.', \''.$avatar.'\')';
$ret = pg_query($DB, $query);

if($ret === false)
{
	header('location: ../signup.php?error');
	die();
}

// Si tout a fonctionné, finalisation de l'upload de l'avatar
if($avatar_submited)
	move_uploaded_file($_FILES['avatar']['tmp_name'], '../avatars/'.$avatar);

header('location: index.php');

?>
