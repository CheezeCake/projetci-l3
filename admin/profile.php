<?php include 'verif_logged.php'; ?>

<!DOCTYPE html>
<html>
	<head>
		<title>Profil</title>
		<link rel="stylesheet" href="../css/header.css">
		<link rel="stylesheet" href="../css/body.css">
		<link rel="stylesheet" href="../css/footer.css">
		<link rel="stylesheet" href="../js/dropit/dropit.css">
		<script type="text/javascript" src="../js/jquery-1.8.3.min.js"></script>
		<script type="text/javascript" src="../js/dropit/dropit.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	    $('.menu').dropit();
});
</script>
	</head>
	<body id="page-body">
	<!-- header -->
<?php include 'header.php'; ?>
	<!-- /header -->
		<section>
			<div class="box" id="account-box">
				<table>
					<tr>
						<td class="title-section"><a href="#">Informations</a></td>
						<td>
							<div id="infos-section">
								<label>Identifiant: </label><?php echo $_SESSION['login']; ?>
							</div>
						</td>
					</tr>
					<tr>
						<td class="title-section"><a href="#">Avatar</a></td>
						<td>
							<div class="section">
								<form action="update_profile.php?avatar" enctype="multipart/form-data" method="post">
									<img src="../avatars/<?php echo $_SESSION['avatar']; ?>" alt="avatar" width="150" height="150">
									<hr>
									<label>Changer d'avatar: </label><input type="file" name="avatar">
									<input type="submit" value="changer avatar">
								</form>
								<hr>
								<a href="update_profile.php?delavatar">Supprimer l'avatar</a>
							</div>
						</td>
					</tr>
					<tr>
						<td class="title-section"><a href="#">Mot de passe</a></td>
						<td>
							<div class="section">
								<form action="update_profile.php" method="post">
									<table>
										<tr>
											<td><label>Ancien mot de passe</label></td><td><input type="text" name="oldpassword"></td>
										</tr>
										<tr>
											<td><label>Nouveau mot de passe</label></td><td><input type="text" name="newpassword"></td>
										</tr>
										<tr>
											<td><label>Confimation nouveau mot de passe</label></td><td><input type="text" name="newpassword1"></td>
										</tr>
										<tr>
											<td></td><td><input type="submit" value="changer mot de passe"></td>
										</tr>
									</table>
								</form>
							</div>
						</td>
					</tr>
				</table>
			</div>
		</section>
	</body>
</html>

